package ru.t1.malyugin.tm.api.service;

import ru.t1.malyugin.tm.enumerated.RoleType;
import ru.t1.malyugin.tm.model.User;

import java.util.Collection;
import java.util.Optional;

public interface IUserService {

    void create(String login, String password, RoleType roleType);

    void create(User user);

    void edit(User user);

    Optional<User> findByLogin(String login);

    Optional<User> findById(String id);

    boolean existsByLogin(String login);

    Collection<User> findAll();

}