package ru.t1.malyugin.tm.configuration;

import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.t1.malyugin.tm.api.endpoint.IProjectSoapEndpoint;
import ru.t1.malyugin.tm.api.endpoint.ITaskSoapEndpoint;
import ru.t1.malyugin.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.t1.malyugin.tm.endpoint.soap.TaskSoapEndpoint;

import javax.xml.ws.Endpoint;

@Configuration
public class WebApplicationConfiguration {

    @Bean
    public SpringBus cxf() {
        return new SpringBus();
    }

    @Bean
    public IProjectSoapEndpoint projectSoapEndpoint() {
        return new ProjectSoapEndpoint();
    }

    @Bean
    public ITaskSoapEndpoint taskSoapEndpoint() {
        return new TaskSoapEndpoint();
    }

    @Bean
    public Endpoint projectEndpointRegistry(
            final IProjectSoapEndpoint projectSoapEndpoint,
            final SpringBus springBus
    ) {
        final EndpointImpl endpoint = new EndpointImpl(springBus, projectSoapEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointRegistry(
            final ITaskSoapEndpoint taskSoapEndpoint,
            final SpringBus springBus
    ) {
        final EndpointImpl endpoint = new EndpointImpl(springBus, taskSoapEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

}