package ru.t1.malyugin.tm.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.t1.malyugin.tm.api.endpoint.IAuthRestEndpoint;
import ru.t1.malyugin.tm.api.service.IUserService;
import ru.t1.malyugin.tm.model.Result;
import ru.t1.malyugin.tm.model.User;

@RestController
@RequestMapping("/api/auth")
public class AuthRestEndpoint implements IAuthRestEndpoint {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private IUserService userService;

    @Override
    @PostMapping("/login")
    public Result login(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password
    ) {
        try {
            final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (final Exception e) {
            e.printStackTrace();
            return new Result(e);
        }
    }

    @Override
    @PostMapping("/logout")
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

    @Override
    @GetMapping(value = "/profile", produces = "application/json")
    public User profile() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();
        return userService.findByLogin(authentication.getName()).orElse(null);
    }

}