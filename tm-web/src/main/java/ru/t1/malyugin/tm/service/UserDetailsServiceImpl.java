package ru.t1.malyugin.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.t1.malyugin.tm.api.service.IUserService;
import ru.t1.malyugin.tm.enumerated.RoleType;
import ru.t1.malyugin.tm.model.CustomUser;
import ru.t1.malyugin.tm.model.Role;
import ru.t1.malyugin.tm.model.User;

import javax.annotation.PostConstruct;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private IUserService userService;

    @Override
    public UserDetails loadUserByUsername(
            final String username
    ) throws UsernameNotFoundException {
        final User user = userService.findByLogin(username).orElseThrow(() -> new UsernameNotFoundException(username));

        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .password(user.getPasswordHash())
                .roles(user.getRoles().stream()
                        .map(Role::toString)
                        .toArray(String[]::new))
                .build()).withUserId(user.getId());
    }

    @PostConstruct
    private void init() {
        userService.create("admin", "admin", RoleType.ADMINISTRATOR);
        userService.create("test", "test", RoleType.USER);
        userService.create("unitTester", "unitTester", RoleType.USER);
    }

}