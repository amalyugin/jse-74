package ru.t1.malyugin.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.malyugin.tm.api.service.ITaskService;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.repository.TaskRepository;

import java.util.Collection;
import java.util.Random;

@Service
public class TaskService implements ITaskService {

    private final Random random = new Random();
    @Autowired
    private TaskRepository taskRepository;

    @Override
    public Collection<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Collection<Task> findAllForUser(final String userId) {
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    public long count() {
        return taskRepository.count();
    }

    @Override
    public long countForUser(final String userId) {
        return taskRepository.countByUserId(userId);
    }

    @Override
    @Transactional
    public void create() {
        int randomNumber = random.nextInt(101);
        Task task = new Task("T " + randomNumber, "DESC");
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void createForUser(final String userId) {
        int randomNumber = random.nextInt(101);
        Task task = new Task("T " + randomNumber, "DESC");
        task.setUserId(userId);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void add(final Task task) {
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void addForUser(final String userId, final Task task) {
        task.setUserId(userId);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void deleteById(final String id) {
        if (!taskRepository.existsById(id)) return;
        taskRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void deleteByIdForUser(final String userId, final String id) {
        if (!taskRepository.existsByUserIdAndId(userId, id)) return;
        taskRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }

    @Override
    @Transactional
    public void clearForUser(final String userId) {
        taskRepository.deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    public void edit(final Task task) {
        if (!taskRepository.existsById(task.getId())) return;
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void editForUser(final String userId, final Task task) {
        if (!taskRepository.existsByUserIdAndId(userId, task.getId())) return;
        taskRepository.save(task);
    }

    @Override
    public Task findById(final String id) {
        return taskRepository.findById(id).orElse(null);
    }

    @Override
    public Task findByIdForUser(final String userId, final String id) {
        return taskRepository.findByUserIdAndId(userId, id).orElse(null);
    }

}