package ru.t1.malyugin.tm.integration.rest;

import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.t1.malyugin.tm.marker.IntegrationCategory;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.Result;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

@Category(IntegrationCategory.class)
public class ProjectRestEndpointTest {

    private static final String BASE_URL = "http://localhost:8080/api/project/";


    private static final HttpHeaders header = new HttpHeaders();

    private static String sessionId;

    private final Project project1 = new Project("T1", "D1");

    private final Project project2 = new Project("T2", "D2");

    private final Project project3 = new Project("T3", "D3");

    private final Project project4 = new Project("T4", "D4");

    @BeforeClass
    public static void beforeClass() {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "http://localhost:8080/api/auth/login?username=test&password=test";
        final ResponseEntity<Result> response = restTemplate.postForEntity(url, Result.class, Result.class);
        System.out.println(response);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().isSuccess());
        final HttpHeaders headersResponse = response.getHeaders();
        final List<HttpCookie> cookies = HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        sessionId = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName()))
                .findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        header.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        header.setContentType(MediaType.APPLICATION_JSON);
    }

    private static <T> ResponseEntity<T> sendRequest(
            final String url,
            final HttpMethod method,
            final HttpEntity httpEntity,
            final Class<T> responseType
    ) {
        final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, responseType);
    }

    @AfterClass
    public static void logout() {
        final String logoutUrl = "http://localhost:8080/api/auth/logout";
        sendRequest(logoutUrl, HttpMethod.POST, new HttpEntity<>(header), Project.class);
    }

    @Before
    public void initTest() {
        final String url = BASE_URL + "post/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project1, header), Project.class);
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project2, header), Project.class);
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project3, header), Project.class);
    }

    @After
    public void clean() {
        final String url = BASE_URL + "delete/all/";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header), Project.class);
    }

    @Test
    public void saveTest() {
        final String url = BASE_URL + "post/";
        final ResponseEntity<Project> response = sendRequest(url, HttpMethod.POST, new HttpEntity<>(project4, header), Project.class);
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
    }

    @Test
    public void countTest() {
        final String url = BASE_URL + "count/";
        final ResponseEntity<Long> response = sendRequest(url, HttpMethod.GET, new HttpEntity<>(header), Long.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(3, response.getBody().longValue());
    }

    @Test
    public void findAllTest() {
        final String url = BASE_URL + "getAll/";
        final ResponseEntity<List> response = sendRequest(url, HttpMethod.GET, new HttpEntity<>(header), List.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(3, response.getBody().size());
    }

    @Test
    public void deleteAllTest() {
        final String deleteAllUrl = BASE_URL + "delete/all/";
        sendRequest(deleteAllUrl, HttpMethod.DELETE, new HttpEntity<>(header), Long.class);

        final String countUrl = BASE_URL + "count/";
        final ResponseEntity<Long> response = sendRequest(countUrl, HttpMethod.GET, new HttpEntity<>(header), Long.class);
        Assert.assertEquals(0, response.getBody().longValue());
    }

    @Test
    public void findByIdTest() {
        final String url = BASE_URL + "get/" + project1.getId();
        System.out.println(url);
        final ResponseEntity<Project> response = sendRequest(url, HttpMethod.GET, new HttpEntity<>(header), Project.class);
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(project1.getId(), response.getBody().getId());
    }

    @Test
    public void deleteByIdTest() {
        final String url = BASE_URL + "delete/" + project1.getId();
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header), Project.class);
        final String urlFind = BASE_URL + "get/" + project1.getId();
        Assert.assertNull(sendRequest(urlFind, HttpMethod.GET, new HttpEntity<>(header), Project.class).getBody());
    }

}

