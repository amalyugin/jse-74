package ru.t1.malyugin.tm.unit.service;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.t1.malyugin.tm.api.service.ITaskService;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Task;

import java.util.UUID;

@SpringBootTest
@RunWith(SpringRunner.class)
@Category(UnitCategory.class)
public class TaskServiceTest {

    private static final String USER_ID = UUID.randomUUID().toString();

    private final Task task1 = new Task("T1", "T1");

    private final Task task2 = new Task("T2", "T2");
    @Autowired
    private ITaskService taskService;

    @Before
    public void initTest() {
        taskService.addForUser(USER_ID, task1);
        taskService.addForUser(USER_ID, task2);
    }

    @After
    public void clean() {
        taskService.clearForUser(USER_ID);
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, taskService.countForUser(USER_ID));
    }

    @Test
    public void findByIdTest() {
        final Task task = taskService.findByIdForUser(USER_ID, task1.getId());
        Assert.assertEquals(task1.getId(), task.getId());
        Assert.assertEquals(task1.getName(), task.getName());
        Assert.assertEquals(task1.getDescription(), task.getDescription());
        Assert.assertEquals(task1.getStatus(), task.getStatus());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(2, taskService.countForUser(USER_ID));
    }

    @Test
    public void deleteAll() {
        taskService.clearForUser(USER_ID);
        Assert.assertEquals(0, taskService.countForUser(USER_ID));
    }

    public void deleteById() {
        Assert.assertNotNull(taskService.findByIdForUser(USER_ID, task1.getId()));
        taskService.deleteByIdForUser(USER_ID, task1.getId());
        Assert.assertNull(taskService.findByIdForUser(USER_ID, task1.getId()));
    }

    @Test
    public void editTest() {
        final Task task = taskService.findByIdForUser(USER_ID, task1.getId());
        task.setDescription("NEW D");
        task.setName("NEW N");
        taskService.editForUser(USER_ID, task);
        final Task newTask = taskService.findByIdForUser(USER_ID, task1.getId());
        Assert.assertEquals(task.getId(), newTask.getId());
        Assert.assertEquals(task.getName(), "NEW N");
        Assert.assertEquals(task.getDescription(), "NEW D");
    }

}