package ru.t1.malyugin.tm.unit.controller;

import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.malyugin.tm.api.service.IProjectService;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.util.UserUtil;

import java.util.Collection;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class ProjectControllerTest {

    private static final String USERNAME = "unitTester";
    private static final String PASSWORD = "unitTester";
    private final Project project1 = new Project("P1", "P1");
    private final Project project2 = new Project("P2", "P2");
    private final Project project3 = new Project("P3", "P3");
    private final Project project4 = new Project("P4", "P4");
    @Autowired
    private IProjectService projectService;
    @Autowired
    private AuthenticationManager authenticationManager;
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext wac;

    @Before
    public void initTest() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, PASSWORD);
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        projectService.addForUser(UserUtil.getUserId(), project1);
        projectService.addForUser(UserUtil.getUserId(), project2);
    }

    @After
    public void clean() {
        projectService.clearForUser(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void findAllTest() {
        final String url = "/projects";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void createTest() {
        final String url = "/project/create";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        final Collection<Project> projects = projectService.findAllForUser(UserUtil.getUserId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(3, projects.size());
    }

    @Test
    @SneakyThrows
    public void deleteById() {
        final String url = "/project/delete/" + project1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        Assert.assertNull(projectService.findByIdForUser(UserUtil.getUserId(), project1.getId()));
    }

    @Test
    @SneakyThrows
    public void editTest() {
        final String url = "/project/edit/" + project1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }

}