package ru.t1.malyugin.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Project;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class ProjectRestEndpointTest {

    private static final String BASE_URL = "http://localhost:8080/api/project/";
    private static final String USERNAME = "unitTester";
    private static final String PASSWORD = "unitTester";
    private final Project project1 = new Project("P1", "P1");
    private final Project project2 = new Project("P2", "P2");
    private final Project project3 = new Project("P3", "P3");
    private final Project project4 = new Project("P4", "P4");
    @Autowired
    private AuthenticationManager authenticationManager;
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext wac;

    @SneakyThrows
    public void saveProject(final Project project) {
        final String url = BASE_URL + "post";
        String projectJson = new ObjectMapper()
                .writerWithDefaultPrettyPrinter()
                .writeValueAsString(project);

        mockMvc.perform(MockMvcRequestBuilders.post(url).content(projectJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    public Project findById(final String id) {
        final String url = BASE_URL + "get/" + id;
        final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if ("".equals(json)) return null;
        final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Project.class);
    }

    @SneakyThrows
    public List<Project> findAll() {
        final String url = BASE_URL + "getAll";
        final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        final ObjectMapper objectMapper = new ObjectMapper();
        List<Project> res = Arrays.asList(objectMapper.readValue(json, Project[].class));
        for (Project project : res) System.out.println(project.getId());

        return res;
    }

    @Before
    public void initTest() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, PASSWORD);
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        saveProject(project1);
        saveProject(project2);
    }

    @After
    @SneakyThrows
    public void clean() {
        final String url = BASE_URL + "delete/all";
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getByIdTest() {
        final Project project = findById(project1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project1.getId(), project.getId());
    }

    @Test
    public void createTest() {
        saveProject(project3);
        Assert.assertEquals(3, findAll().size());
    }

    @Test
    public void getListTest() {
        Assert.assertEquals(2, findAll().size());
    }

    @Test
    @SneakyThrows
    public void countTest() {
        final String url = BASE_URL + "count";
        final String count = mockMvc.perform(MockMvcRequestBuilders.get(url).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        Assert.assertEquals(new Long(2), new Long(count));
    }

    @Test
    @SneakyThrows
    public void clearTest() {
        final String url = BASE_URL + "delete/all";
        mockMvc.perform(MockMvcRequestBuilders.delete(url).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertEquals(0, findAll().size());
    }

}