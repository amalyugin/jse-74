package ru.t1.malyugin.tm.configuration;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.t1.malyugin.tm.api.service.property.IPropertyService;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Configuration
public class LiquibaseConfiguration {

    @NotNull
    private static final ClassLoaderResourceAccessor ACCESSOR = new ClassLoaderResourceAccessor();

    @Bean
    @NotNull
    public Liquibase liquibase(@NotNull final IPropertyService propertyService) throws IOException, SQLException, DatabaseException {
        @NotNull final String url = propertyService.getDatabaseUrl();
        @NotNull final String username = propertyService.getDatabaseUsername();
        @NotNull final String password = propertyService.getDatabasePassword();
        @NotNull final String filename = propertyService.getLiquibaseConfigPath();

        final Connection connection = DriverManager.getConnection(url, username, password);
        final JdbcConnection jdbcConnection = new JdbcConnection(connection);
        @Nullable Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);

        return new Liquibase(filename, ACCESSOR, database);
    }

}