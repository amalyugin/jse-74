package ru.t1.malyugin.tm.service.model;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.malyugin.tm.api.service.model.IUserOwnedService;
import ru.t1.malyugin.tm.api.service.model.IUserService;
import ru.t1.malyugin.tm.exception.entity.EntityNotFoundException;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.UserIdEmptyException;
import ru.t1.malyugin.tm.exception.user.UserNotFoundException;
import ru.t1.malyugin.tm.model.AbstractUserOwnedModel;
import ru.t1.malyugin.tm.model.User;
import ru.t1.malyugin.tm.repository.model.AbstractUserOwnedRepository;

import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel> extends AbstractService<M>
        implements IUserOwnedService<M> {

    @NotNull
    @Autowired
    protected IUserService userService;

    @NotNull
    @Override
    protected abstract AbstractUserOwnedRepository<M> getRepository();

    @Override
    public long getSize(@Nullable final String userId) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        @Nullable final User user = userService.findOneById(userId.trim());
        if (user == null) throw new UserNotFoundException();
        return getRepository().countByUser(user);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        @Nullable final User user = userService.findOneById(userId.trim());
        if (user == null) throw new UserNotFoundException();
        getRepository().deleteByUser(user);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final User user = userService.findOneById(userId.trim());
        if (user == null) throw new UserNotFoundException();
        return getRepository().findByUserAndId(user, id.trim()).orElse(null);
    }

    @Override
    public @NotNull List<M> findAll(@Nullable final String userId) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        @Nullable final User user = userService.findOneById(userId.trim());
        if (user == null) throw new UserNotFoundException();
        return getRepository().findAllByUser(user);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        getRepository().deleteById(id.trim());
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final M model) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (model == null) throw new EntityNotFoundException();
        @Nullable final User user = userService.findOneById(userId.trim());
        if (user == null) throw new UserNotFoundException();
        getRepository().delete(model);
    }

}