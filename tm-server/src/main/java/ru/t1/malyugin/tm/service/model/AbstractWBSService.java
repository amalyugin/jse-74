package ru.t1.malyugin.tm.service.model;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.malyugin.tm.api.service.model.IWBSService;
import ru.t1.malyugin.tm.enumerated.EntitySort;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.UserIdEmptyException;
import ru.t1.malyugin.tm.exception.user.UserNotFoundException;
import ru.t1.malyugin.tm.model.AbstractWBSModel;
import ru.t1.malyugin.tm.model.User;
import ru.t1.malyugin.tm.repository.model.AbstractWBSRepository;

import java.util.List;

public abstract class AbstractWBSService<M extends AbstractWBSModel> extends AbstractUserOwnedService<M>
        implements IWBSService<M> {

    @NotNull
    @Override
    protected abstract AbstractWBSRepository<M> getRepository();

    @NotNull
    @Override
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final EntitySort sort
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        @Nullable final User user = userService.findOneById(userId.trim());
        if (user == null) throw new UserNotFoundException();
        if (sort == null) return getRepository().findAllByUser(user);
        return getRepository().findAllByUser(user, Sort.by(Sort.Direction.ASC, sort.getName()));
    }

    @Override
    @Transactional
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final M model = findOneById(userId.trim(), id);
        if (model == null) return;
        if (status != null) model.setStatus(status);
        update(model);
    }

}