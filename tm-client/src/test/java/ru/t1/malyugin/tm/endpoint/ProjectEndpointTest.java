package ru.t1.malyugin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.dto.model.ProjectDTO;
import ru.t1.malyugin.tm.dto.request.project.*;
import ru.t1.malyugin.tm.dto.request.user.UserLoginRequest;
import ru.t1.malyugin.tm.dto.request.user.UserLogoutRequest;
import ru.t1.malyugin.tm.dto.response.project.ProjectShowListResponse;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.marker.SoapCategory;

import java.util.List;

import static ru.t1.malyugin.tm.TestData.*;

@Category(SoapCategory.class)
public final class ProjectEndpointTest {

    @BeforeClass
    public static void setToken() {
        @NotNull final String login = PROPERTY_SERVICE.getSoapLogin();
        @NotNull final String pass = PROPERTY_SERVICE.getSoapPass();
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(login, pass);
        @Nullable final String userToken = AUTH_ENDPOINT.login(loginRequest).getToken();
        Assert.assertNotNull(userToken);
        TOKEN_SERVICE_USUAL.setToken(userToken);
    }

    @AfterClass
    public static void clearToken() {
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(TOKEN_SERVICE_USUAL.getToken());
        AUTH_ENDPOINT.logout(request);
    }

    @NotNull
    private String getUserToken() {
        return TOKEN_SERVICE_USUAL.getToken();
    }

    @Before
    public void initTest() {
        final int numberOfProjects = 3;
        for (int i = 1; i <= numberOfProjects; i++) {
            @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(
                    getUserToken(),
                    "NAME " + i,
                    "D " + i
            );
            @Nullable final ProjectDTO project = PROJECT_ENDPOINT.creteProject(request).getProject();
            Assert.assertNotNull(project);
            PROJECT_LIST.add(project);
        }
    }

    @After
    public void after() {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(getUserToken());
        Assert.assertNotNull(PROJECT_ENDPOINT.clearProject(request));
        PROJECT_LIST.clear();
    }

    @Test
    public void testChangeStatusById() {
        @NotNull final Status status = Status.COMPLETED;
        for (@NotNull final ProjectDTO project : PROJECT_LIST) {
            @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(
                    getUserToken(),
                    project.getId(),
                    status
            );
            PROJECT_ENDPOINT.changeProjectStatusById(request);
        }
    }

    @Test
    public void testCompleteById() {
        for (@NotNull final ProjectDTO project : PROJECT_LIST) {
            @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(
                    getUserToken(),
                    project.getId()
            );
            PROJECT_ENDPOINT.completeProjectById(request);
        }
    }

    @Test
    public void testStartById() {
        for (@NotNull final ProjectDTO project : PROJECT_LIST) {
            @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(
                    getUserToken(), project.getId()
            );
            PROJECT_ENDPOINT.startProjectById(request);
        }
    }

    @Test
    public void testRemoveById() {
        for (@NotNull final ProjectDTO project : PROJECT_LIST) {
            @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(
                    getUserToken(), project.getId()
            );
            PROJECT_ENDPOINT.removeProjectById(request);
            @NotNull final ProjectShowByIdRequest requestProject = new ProjectShowByIdRequest(
                    getUserToken(), project.getId());
            Assert.assertNull(PROJECT_ENDPOINT.showProjectById(requestProject).getProject());
        }
    }

    @Test
    public void testShowById() {
        for (@NotNull final ProjectDTO project : PROJECT_LIST) {
            @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(
                    getUserToken(), project.getId()
            );
            @Nullable final ProjectDTO actualProject = PROJECT_ENDPOINT.showProjectById(request).getProject();
            Assert.assertNotNull(actualProject);
            Assert.assertEquals(project.getId(), actualProject.getId());
        }
    }

    @Test
    public void testShowList() {
        @NotNull final ProjectShowListRequest request = new ProjectShowListRequest(getUserToken(), null);
        @Nullable final ProjectShowListResponse response = PROJECT_ENDPOINT.showProjectList(request);
        Assert.assertNotNull(response.getProjectList());
        @NotNull List<ProjectDTO> actualList = response.getProjectList();
        Assert.assertEquals(PROJECT_LIST.size(), actualList.size());
    }

    @Test
    public void testClear() {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(getUserToken());
        Assert.assertNotNull(PROJECT_ENDPOINT.clearProject(request));
        @NotNull final ProjectShowListRequest requestList = new ProjectShowListRequest(getUserToken(), null);
        Assert.assertNull(PROJECT_ENDPOINT.showProjectList(requestList).getProjectList());
    }

    @Test
    public void testCreate() {
        @NotNull final String name = "TEST_NAME";
        @NotNull final String description = "TEST_DESC";
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(
                getUserToken(), name, description
        );
        @Nullable final ProjectDTO project = PROJECT_ENDPOINT.creteProject(request).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());
    }

    @Test
    public void testUpdateById() {
        @NotNull final String name = "NEW_NAME";
        @NotNull final String description = "NEW_DESCRIPTION";
        @NotNull final ProjectDTO project = PROJECT_LIST.get(0);
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(
                getUserToken(), project.getId(), name, description);
        Assert.assertNotNull(PROJECT_ENDPOINT.updateProjectById(request));

        @NotNull final ProjectShowByIdRequest requestProject = new ProjectShowByIdRequest(
                getUserToken(), project.getId());
        @Nullable final ProjectDTO actualProject = PROJECT_ENDPOINT.showProjectById(requestProject).getProject();
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(name, actualProject.getName());
        Assert.assertEquals(description, actualProject.getDescription());
    }

}