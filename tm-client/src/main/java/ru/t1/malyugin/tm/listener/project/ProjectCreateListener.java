package ru.t1.malyugin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.malyugin.tm.event.ConsoleEvent;
import ru.t1.malyugin.tm.util.TerminalUtil;

@Component
public final class ProjectCreateListener extends AbstractProjectListener {

    @NotNull
    private static final String NAME = "project-create";

    @NotNull
    private static final String DESCRIPTION = "Create new project";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectCreateListener.getName() == #event.message")
    public void handleConsoleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[CREATE PROJECT]");

        System.out.print("ENTER PROJECT NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER PROJECT DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();

        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(getToken(), name, description);
        projectEndpoint.creteProject(request);
    }

}