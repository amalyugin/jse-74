package ru.t1.malyugin.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum EntitySort {

    BY_NAME("name", "Sort by name"),
    BY_STATUS("status", "Sort by status"),
    BY_CREATED("created", "Sort by created");

    @NotNull
    @Getter
    private final String name;

    @NotNull
    @Getter
    private final String displayName;

    EntitySort(
            @NotNull final String name,
            @NotNull final String displayName
    ) {
        this.name = name;
        this.displayName = displayName;
    }

    @NotNull
    public static String renderValuesList() {
        @NotNull final StringBuilder result = new StringBuilder();
        final int size = EntitySort.values().length;
        for (int i = 0; i < size; i++) {
            result.append(String.format("%d - %s, ", i, EntitySort.values()[i].displayName));
        }
        return result.toString();
    }

    @Nullable
    public static EntitySort getSortByIndex(final Integer index) {
        if (index == null || index < 0 || index >= EntitySort.values().length) return null;
        return EntitySort.values()[index];
    }

}